# Pentest Observations Drafter (pod)

This repository provides an example how I'm generating my reports.
I've been developing a similar structure for a client of ours, and I'm hereby open sourcing my work.

# Purpose of this repository

This repository is mainly for my own convenience, because our internal management system lacks the collaboration experience we need and find in Git instead.
I completely reshaped the way we do penetration testing (for one client so far), hopefully to everyone's benefit (testers, and reviewers alike).
I made this happen by identifying a few inconveniences that made my life as a tester horrible, such as:

* Our internal management system is an *archiver designed for the audit world*, for compliance purposes - it is *not a collaboration tool*.
  It's an *absolute nightmare to collaborate with*.
  To name a few reasons why this is not a collaboration tool:
  * No conflict resolution;
  * No file comparison (e.g. git diff);
  * No way to tell who did what to what file and when (e.g. `git status`; `git blame`);
  * No way to roll back to previous sync points (e.g. `git reset`);
  * No branches support (i.e. you always work on the master branch)
  * No cherry picking individual changes, and so on...
  * Honestly, just send me back to the dark ages already...
* *Excel* is always familiar to the client.
  No matter who you're dealing with - they always know how to open and read a spreadsheet.
  However, this is also horrible to collaborate with in terms of picking up review comments and nit-picking about trivial mark-up details.
  There's a reason humanity invented tons of tools that avoid distracting the author with tedious markup and eye-candy trivialities.
  I firmly believe the author should strictly focus on the actual content.
  Tools like troff, TeX, LaTex, XeTeX, Markdown, reStructuredText, and AsciiDoc identify this and are developed for exactly this purpose.
  Furthermore, because Excel uses binary formats, you need a complete office suite to open XLS, XLSX, ODT, FODT and what not.
  This should be much simpler, and is much simpler when moving to a different tool chain.

How I made it better:

* Choosing a different tool chain. Our internal management system will only be used to *archive* our work - not to collaborate with.
  Instead, we use *Git*!
  We use our new Git repo to collaborate with for the entire engagement.
  This means all our documents are drafted in here.
  And as soon as the pentest is done, we create a single tarball of the tree, and then we store it in our internal engagement system.
  That's all.
* No more Excel!
  We use *AsciiDoc*!
  Why not Markdown?
  Because MArkdown is quite limited in its feature set - AsciiDoc is much more powerful.
  But like MArkdown, AsciiDoctor uses a non-binary, flat and human readable format.
  This is great to work with, because it's easy to edit and easy to keep track of in version control.
  All our documents are drafted in this format! Briefings, work programs, evidence files, issues, summaries, and reports.

The above is an absolute time-saver for testers (IMHO).
Sure, one would need to learn Git and AsciiDoc first, but this is a trivial investment which is returned after a few engagements.
This I know, because I've been training other pentesters whom I'm coordinating as of a few months.
It's easier to collaborate with, and easier to review the work.
And since most techies like Git anyway, it has also made pentesting more fun!

# Requirements

You will need the following tools to convert AsciiDoctor files to HTML, and then to PDF:

1. asciidoctor (dev-ruby/asciidoctor);
2. asciidoctor-pdf (dev-ruby/asciidoctor-pdf);

The following tools are (somewhat) optional.
They allow for properly rendered syntax highlighting when printing source code, and to automatically render AsciiDoc files upon write to disk.
The latter is achieved by using the Guardfile within the root of each engagement, and executing `guard` from within the engagement root.

1. coderay (dev-ruby/coderay);
2. guard (dev-ruby/guard);
3. guard-shell (dev-ruby/guard-shell).

# Example

cd to the base of the current engagement:

```
$ cd client/acme/anvil
```

From here you can edit the AsciiDoc files in _source/adoc_.
You can save screen shots in _source/img/evidences_.
Afterwards, you can either use `asciidoctor-pdf` manually, or use `guard` to automatically generate the PDFs.

## Automatically

Make sure to have `guard` and `guard-shell` installed.
Start by running `guard` in the root of the engagement.
After the Guard shell is loaded, you can edit any of the AsciiDoc files referenced in the Guardfile.
Upon writing your changes to disk (i.e. saving your file) Guard will automatically render the document.

## Manually

Let's render _source/adoc/wp.adoc_ as an example:

```
asciidoctor-pdf -D work-papers source/adoc/wp.adoc
```

And that's it.
Repeat so for all other documents that need to be generated.

# Appendix A: why not Excel?

We need a non-binary format that is easy to track in Git.
Otherwise, helpful features like `git diff` are rendered completely useless due to using a binary format.
There have been projects that try to workaround this, but they did not comply with our needs.
Yes, we did try them and we'll quickly go over them:

1. xls2txt (https://pypi.org/project/pyExcelerator/):
   Can be used to trigger a bit more readable `git diff` output by adding triggers in `.gitattributes` and `.git/config` in the root of the repository.
   However, this is still not ideal because the commits themselves are still horrible to keep track of.
   This also only works for legacy XLS (Office 97 - 2003).
2. xlsx2txt (https://github.com/shibukawa/xlsx2txt):
   Same deal as with xlsx2txt.
   The only difference being that it works for the more modern XLSX format.
3. Switching to CSV:
   Flat and non-binary format, so this is much more easier to keep track of in Git.
   However, you'll lose all markup and formatting to generate a nice deliverable.
   Nice in version control; horrible for client deliverables.
4. Switching to FODT:
   Same deal as with CSV.
   Probably even worse in version control, because of so much XML stuff that keeps changing whenever you make even the most trivial edits.
   Horrible in version control; horrible for client deliverables.
5. xltrail (https://www.xltrail.com/):
   Commercial proprietary BS; only for Windows.
   Utter $H!t.
6. Switching to Markdown:
   I love Markdown, because it's so easy to work with in Git.
   It's readable, and can easily be converted to almost all other imaginable formats such as by using `pandoc`.
   However, you cannot store markup details, such as colours, let alone cell background colours.
   And we need this for our deliverables.
   Markdown is very easy, and favourable in version control, but sadly does not live up to our client's standards.

Experimenting with the above took me a whole day to realise what we need in both version control.
I found *AsciiDoc* to be a working alternative.

# Appendix B: why AsciiDoc?

AsciiDoc is similar to Markdown in that it uses a non-binary, flat format and its syntax is human readable.
It's far more feature-rich than Markdown, and can also be converted to any number of formats, such as by using `asciidoctor`, `asciidoctor-pdf`, or even `pandoc`.
Great in version control, and can be used to generate pretty deliverables.
